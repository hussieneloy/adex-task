package task;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Timestamp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import task.model.Customer;
import task.model.IPBlacklist;
import task.model.UABlacklist;
import task.service.CustomerService;
import task.service.IPBlacklistService;
import task.service.RequestService;
import task.service.StatService;
import task.service.UABlacklistService;

@ExtendWith(MockitoExtension.class)
public class RequestServiceTest {
	@Mock
	CustomerService customerService;
	@Mock
	StatService statService;
	@Mock
	IPBlacklistService ipBlackService;
	@Mock
	UABlacklistService uaBlacklistService;
	@InjectMocks
	RequestService requestService;
	
	@Test
	void malformedRequestTest() {
		String request = "foo";
		String response = requestService.handleRequest(request);
		assertEquals("Malformed Json Request for unknown Customer", response);
	}
	
	@Test
	void missingCustomerIDTest() {
		String request = "{\"name\":\"\",\"age\":\"20\"}";
		String response = requestService.handleRequest(request);
		assertEquals("The request is missing a customer ID", response);
	}
	
	@Test
	void nonExistentCustomerTest() {
		Mockito.when(customerService.findCustomerByID(1)).thenReturn(null);
		String request = "{\"customerID\":1}";
		String response = requestService.handleRequest(request);
		assertEquals("The customer is not present in the database", response);
	}
	
	@Test
	void missingFieldsTest() {
		Customer customer = new Customer();
		customer.setId(1);
		Mockito.when(customerService.findCustomerByID(1)).thenReturn(customer);
		String request = "{\"customerID\":1, \"timestamp\":1500}";
		String response = requestService.handleRequest(request);
		assertEquals("The request is missing some fields", response);
		Mockito.verify(statService, Mockito.times(1)).storeInvalidRequest(1, new Timestamp(1500 * 1000L));
	}
	
	@Test
	void inactiveCustomerTest() {
		Customer customer = new Customer();
		customer.setId(1);
		customer.setActive(false);
		Mockito.when(customerService.findCustomerByID(1)).thenReturn(customer);
		String request = "{\"customerID\":1,\"tagID\":2,\"userID\":\"aaaaaaaa-bbbb-cccc-1111-222222222222\","
				+ "\"remoteIP\":\"0.0.0.0\",\"timestamp\":1500000000}";
		String response = requestService.handleRequest(request);
		assertEquals("The customer is deactivated", response);
		Mockito.verify(statService, Mockito.times(1)).storeInvalidRequest(1, new Timestamp(1500000000 * 1000L));
	}
	
	@Test
	void invalidIPTest() {
		Customer customer = new Customer();
		customer.setId(1);
		customer.setActive(true);
		Mockito.when(customerService.findCustomerByID(1)).thenReturn(customer);
		String request = "{\"customerID\":1,\"tagID\":2,\"userID\":\"aaaaaaaa-bbbb-cccc-1111-222222222222\","
				+ "\"remoteIP\":\"false IP\",\"timestamp\":1500000000}";
		String response = requestService.handleRequest(request);
		assertEquals("Invalid input remote IP", response);
		Mockito.verify(statService, Mockito.times(1)).storeInvalidRequest(1, new Timestamp(1500000000 * 1000L));
	}
	
	@Test
	void blacklistedIPTest() {
		Customer customer = new Customer();
		customer.setId(1);
		customer.setActive(true);
		Mockito.when(customerService.findCustomerByID(1)).thenReturn(customer);
		IPBlacklist ipBlacklist = new IPBlacklist();
		ipBlacklist.setIp(0L);
		Mockito.when(ipBlackService.getByIP(0L)).thenReturn(ipBlacklist);
		String request = "{\"customerID\":1,\"tagID\":2,\"userID\":\"aaaaaaaa-bbbb-cccc-1111-222222222222\","
				+ "\"remoteIP\":\"0.0.0.0\",\"timestamp\":1500000000}";
		String response = requestService.handleRequest(request);
		assertEquals("Remote IP is blacklisted", response);
		Mockito.verify(statService, Mockito.times(1)).storeInvalidRequest(1, new Timestamp(1500000000 * 1000L));
	}
	
	@Test
	void blacklistedUATest() {
		Customer customer = new Customer();
		customer.setId(1);
		customer.setActive(true);
		customer.setName("Bad Agent");
		Mockito.when(customerService.findCustomerByID(1)).thenReturn(customer);
		Mockito.when(ipBlackService.getByIP(0L)).thenReturn(null);
		UABlacklist uaBlacklist = new UABlacklist();
		uaBlacklist.setUa("Bad Agent");
		Mockito.when(uaBlacklistService.getUAByName("Bad Agent")).thenReturn(uaBlacklist);
		String request = "{\"customerID\":1,\"tagID\":2,\"userID\":\"aaaaaaaa-bbbb-cccc-1111-222222222222\","
				+ "\"remoteIP\":\"0.0.0.0\",\"timestamp\":1500000000}";
		String response = requestService.handleRequest(request);
		assertEquals("User Agent is blacklisted", response);
		Mockito.verify(statService, Mockito.times(1)).storeInvalidRequest(1, new Timestamp(1500000000 * 1000L));
	}

	@Test
	void goodRequestTest() {
		Customer customer = new Customer();
		customer.setId(1);
		customer.setActive(true);
		customer.setName("Good Agent");
		Mockito.when(customerService.findCustomerByID(1)).thenReturn(customer);
		Mockito.when(ipBlackService.getByIP(0L)).thenReturn(null);
		Mockito.when(uaBlacklistService.getUAByName("Good Agent")).thenReturn(null);
		String request = "{\"customerID\":1,\"tagID\":2,\"userID\":\"aaaaaaaa-bbbb-cccc-1111-222222222222\","
				+ "\"remoteIP\":\"0.0.0.0\",\"timestamp\":1500000000}";
		String response = requestService.handleRequest(request);
		assertEquals("A valid Request is stored for: Good Agent", response);
		Mockito.verify(statService, Mockito.times(1)).storeValidRequest(1, new Timestamp(1500000000 * 1000L));
	}



	

}
