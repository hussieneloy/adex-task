package task;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import task.dto.StatResponse;
import task.model.Customer;
import task.model.Stat;
import task.repository.StatRepository;
import task.service.CustomerService;
import task.service.StatService;

@ExtendWith(MockitoExtension.class)
public class StatServiceTest {
	@Mock
	StatRepository statRepository;
	@Mock
	CustomerService customerService;
	@InjectMocks
	StatService statService;
	
	@Test
	void storeNewValidRequest() {
		Timestamp start = new Timestamp(0L);
		Mockito.when(statRepository.getByCustomerAndTimestamp(1, start)).thenReturn(new ArrayList<Stat>());
		Customer customer = new Customer();
		customer.setId(1);
		Mockito.when(customerService.findCustomerByID(1)).thenReturn(customer);
		Stat newStat = statService.storeValidRequest(1, start);
		assertEquals(1L, newStat.getRequestCount());
		assertEquals(0L, newStat.getInvalidCount());
		Mockito.verify(statRepository, Mockito.times(1)).save(newStat);
	}
	
	@Test
	void storeNewInvalidRequest() {
		Timestamp start = new Timestamp(0L);
		Mockito.when(statRepository.getByCustomerAndTimestamp(1, start)).thenReturn(new ArrayList<Stat>());
		Customer customer = new Customer();
		customer.setId(1);
		Mockito.when(customerService.findCustomerByID(1)).thenReturn(customer);
		Stat newStat = statService.storeInvalidRequest(1, start);
		assertEquals(0L, newStat.getRequestCount());
		assertEquals(1L, newStat.getInvalidCount());
		Mockito.verify(statRepository, Mockito.times(1)).save(newStat);
	}
	
	@Test
	void storeExistingValidRequest() {
		Timestamp start = new Timestamp(0L);
		Stat oldStat = new Stat();
		oldStat.setInvalidCount(3L);
		oldStat.setRequestCount(5L);
		List<Stat> oldStats = new ArrayList<Stat>();
		oldStats.add(oldStat);
		Mockito.when(statRepository.getByCustomerAndTimestamp(1, start)).thenReturn(oldStats);
		Stat newStat = statService.storeValidRequest(1, start);
		assertEquals(6L, newStat.getRequestCount());
		assertEquals(3L, newStat.getInvalidCount());
		Mockito.verify(statRepository, Mockito.times(1)).save(newStat);
	}
	
	@Test
	void storeExistingInvalidRequest() {
		Timestamp start = new Timestamp(0L);
		Stat oldStat = new Stat();
		oldStat.setInvalidCount(3L);
		oldStat.setRequestCount(5L);
		List<Stat> oldStats = new ArrayList<Stat>();
		oldStats.add(oldStat);
		Mockito.when(statRepository.getByCustomerAndTimestamp(1, start)).thenReturn(oldStats);
		Stat newStat = statService.storeInvalidRequest(1, start);
		assertEquals(5L, newStat.getRequestCount());
		assertEquals(4L, newStat.getInvalidCount());
		Mockito.verify(statRepository, Mockito.times(1)).save(newStat);
	}
	
	@Test
	void getCustomerAndDayStat() {
		Timestamp start = new Timestamp(0L);
		Timestamp end = new Timestamp((24 * 60 * 60 * 1000) - 1L);
		Mockito.when(statRepository.getCustomerInvalidRequests(start, end, 1)).thenReturn(0L);
		Mockito.when(statRepository.getCustomerValidRequests(start, end, 1)).thenReturn(1L);
		Mockito.when(statRepository.getTotalInvalidRequests(start, end)).thenReturn(2L);
		Mockito.when(statRepository.getTotalValidRequests(start, end)).thenReturn(3L);
		System.out.println(start);
		StatResponse statResponse = statService.getStat(1, "1970-1-1");
		assertEquals("1970-01-01", statResponse.getDay().toString());
		assertEquals(0L, statResponse.getCustomerInvalidRequests());
		assertEquals(1L, statResponse.getCustomerValidRequests());
		assertEquals(2L, statResponse.getTotalInvalidRequests());
		assertEquals(3L, statResponse.getTotalValidRequests());
	}
}
