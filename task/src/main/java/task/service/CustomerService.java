package task.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import task.model.Customer;
import task.repository.CustomerRepository;

@Service
public class CustomerService {
		@Autowired
		CustomerRepository customerRepository;
		
		public List<Customer> getAllCustomers() {
			return customerRepository.findAll();
		}
		
		public Customer createCustomer(Customer customer) {
			return customerRepository.save(customer);
		}
		
		public Customer findCustomerByID(Integer id) {
			return customerRepository.findById(id).orElse(null);
		}
}
