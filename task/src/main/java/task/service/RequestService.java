package task.service;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import task.model.Customer;
import task.model.IPBlacklist;
import task.model.UABlacklist;

@Service
public class RequestService {
	@Autowired
	CustomerService customerService;
	@Autowired
	StatService statService;
	@Autowired
	IPBlacklistService ipBlacklistService;
	@Autowired
	UABlacklistService uaBlacklistService;
	
	static final Logger log = Logger.getLogger(RequestService.class.getName());
	
	public String handleRequest(String rawJson) {
		
		log.log(Level.INFO, "Checking for Malformed Json");
		
		if (malformedJson(rawJson)) {
			return "Malformed Json Request for unknown Customer";
		}
		
		JsonNode requestNode = mapJsonRequest(rawJson);
		
		log.log(Level.INFO, "Transforming JsonNode into a Map");
		Map<String, Object> requestMap = convertNodetoMap(requestNode);
		
		log.log(Level.INFO, "Checking if the request is missing a customer ID");
		if (missingCustomer(requestMap)) {
			return "The request is missing a customer ID";
		}
		
		log.log(Level.INFO, "Checking if the customer Id is present in the system");
		if (nonExistentCustomer(requestMap)) {
			return "The customer is not present in the database";
		}
		
		Integer customerId = (Integer) requestMap.get("customerID");
		
		Integer rawTimeStamp = (Integer) requestMap.get("timestamp");
		
		log.log(Level.INFO, "Converting input timestamp");
		Timestamp timeStamp = (rawTimeStamp == null)? 
				new Timestamp(System.currentTimeMillis()):new Timestamp((long) rawTimeStamp * 1000L);
		
		log.log(Level.INFO, "Checking if any field is missing");
		if (missingAnyField(requestMap)) {
			statService.storeInvalidRequest(customerId, timeStamp);
			return "The request is missing some fields";
		}
		
		log.log(Level.INFO, "Getting a Customer from the databse");
		Customer customer = customerService.findCustomerByID(customerId);
		
		log.log(Level.INFO, "Checking if the customer is active");
		if(!customer.isActive()) {
			statService.storeInvalidRequest(customerId, timeStamp);
			return "The customer is deactivated";
		}
		
		log.log(Level.INFO, "Converting input remote IP into a numeric form");
		String remoteIP = (String) requestMap.get("remoteIP");
		Long longIP = convertStringIpToLong(remoteIP);
		
		log.log(Level.INFO, "Checking if the remote IP is valid");
		if (longIP == null) {
			statService.storeInvalidRequest(customerId, timeStamp);
			return "Invalid input remote IP";
		}
		
		log.log(Level.INFO, "Checking if remote IP is blacklisted");
		IPBlacklist ipBlacklist = ipBlacklistService.getByIP(longIP);
		
		if (ipBlacklist != null) {
			statService.storeInvalidRequest(customerId, timeStamp);
			return "Remote IP is blacklisted";
		}
		
		log.log(Level.INFO, "Checking if the user agent is blacklisted");
		UABlacklist uaBlacklist = uaBlacklistService.getUAByName(customer.getName());
		
		if (uaBlacklist != null) {
			statService.storeInvalidRequest(customerId, timeStamp);
			return "User Agent is blacklisted";
		}
		
		statService.storeValidRequest(customerId, timeStamp);
		
		log.log(Level.INFO, "Processing a successful request");
		processRequest(rawJson);
		return "A valid Request is stored for: " + customer.getName();
	}

	private void processRequest(String rawJson) {
		// TODO Auto-generated method stub
		
	}

	private Long convertStringIpToLong(String remoteIP) {
		try {
			BigInteger num = new BigInteger(InetAddress.getByName(remoteIP).getAddress());
			return num.longValue();
		} catch (UnknownHostException e) {
			return null;
		}
	}

	private boolean missingAnyField(Map<String, Object> requestMap ) {
		return !(requestMap.containsKey("customerID") && requestMap.containsKey("tagID") &&
				requestMap.containsKey("userID") && requestMap.containsKey("remoteIP") &&
				requestMap.containsKey("timestamp"));
	}

	private boolean nonExistentCustomer(Map<String, Object> requestMap ) {
		Integer customerId = (Integer) requestMap.get("customerID");
		Customer customer = customerService.findCustomerByID(customerId);
		return customer == null;
	}

	private boolean missingCustomer(Map<String, Object> requestMap) {
		return !requestMap.containsKey("customerID");
	}

	private Map<String, Object> convertNodetoMap(JsonNode requestNode) {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(requestNode, new TypeReference <Map<String, Object>>() {});
	}

	private boolean malformedJson(String rawJson) {
		return mapJsonRequest(rawJson) == null;
	}
	
	private JsonNode mapJsonRequest(String rawJson) {
		log.log(Level.INFO, "Convert raw Json into Json Node.");
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = null;
		try {
			node = mapper.readTree(rawJson);
			return node;
		} catch (JsonMappingException e) {
			return null;
		} catch (JsonProcessingException e) {
			return null;
		}
	}

}
