package task.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import task.model.IPBlacklist;
import task.repository.IPBlacklistRepository;

@Service
public class IPBlacklistService {
	@Autowired
	IPBlacklistRepository ipBlacklistRepository;
	
	public List<IPBlacklist> getAllIps() {
		return ipBlacklistRepository.findAll();
	}
	
	public IPBlacklist createIP(IPBlacklist ipBlacklist) {
		return ipBlacklistRepository.save(ipBlacklist);
	}
	
	public IPBlacklist getByIP(Long ip) {
		return ipBlacklistRepository.findById(ip).orElse(null);
	}

}
