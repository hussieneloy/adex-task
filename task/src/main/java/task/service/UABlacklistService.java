package task.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import task.model.UABlacklist;
import task.repository.UABlacklistRepository;

@Service
public class UABlacklistService {
	@Autowired
	UABlacklistRepository uaBlacklistRepository;
	
	public List<UABlacklist> getAllUAs() {
		return uaBlacklistRepository.findAll();
	}
	
	public UABlacklist createUA(UABlacklist uaBlacklist) {
		return uaBlacklistRepository.save(uaBlacklist);
	}

	public UABlacklist getUAByName(String name) {
		return uaBlacklistRepository.findById(name).orElse(null);
	}
}
