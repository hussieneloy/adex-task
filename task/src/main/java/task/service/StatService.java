package task.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import task.dto.StatResponse;
import task.model.Stat;
import task.repository.StatRepository;

@Service
public class StatService {
	@Autowired
	StatRepository statRepository;
	@Autowired
	CustomerService customerService;
	
	static final Logger log = Logger.getLogger(StatService.class.getName());

	public Stat storeInvalidRequest(Integer customerId, Timestamp timeStamp) {
		log.log(Level.INFO, "Storing an Invalid request");
		return insertOrUpdateDate(customerId, timeStamp, false);	
	}
	
	public Stat storeValidRequest(Integer customerId, Timestamp timeStamp) {
		log.log(Level.INFO, "Storing a valid request");
		return insertOrUpdateDate(customerId, timeStamp, true);	
	}
	
	public StatResponse getStat(Integer customerId, String rawDay) {
		log.log(Level.INFO, "Setting proper timestamps to the input day");
		Date day = Date.valueOf(rawDay);
		StatResponse response = new StatResponse(customerId, day, 0L, 0L, 0L, 0L);
		long midnight = day.getTime() + (60 * 60 * 1000);
		Timestamp start = new Timestamp(midnight);
		Long duration = (24 * 60 * 60 * 1000) - 1L;
		Timestamp end = new Timestamp(midnight + duration);
		response.setCustomerId(customerId);
		response.setDay(day);
		log.log(Level.INFO, "Getting stats for the day: " + rawDay);
		response.setTotalValidRequests(statRepository.getTotalValidRequests(start, end));
		response.setTotalInvalidRequests(statRepository.getTotalInvalidRequests(start, end));
		log.log(Level.INFO, "Getting stats for the day: " + rawDay + " and customer with ID: " + customerId);
		response.setCustomerValidRequests(statRepository.getCustomerValidRequests(start, end, customerId));
		response.setCustomerInvalidRequests(statRepository.getCustomerInvalidRequests(start, end, customerId));
		return response;
	}

	private Stat insertOrUpdateDate(Integer customerId, Timestamp timeStamp, boolean isValid) {
		List<Stat> oldStats = statRepository.getByCustomerAndTimestamp(customerId, timeStamp);
		
		log.log(Level.INFO, "Checking if the stat already exists");
		if (oldStats.isEmpty()) {
			return insertStat(customerId, timeStamp, isValid);
		}
		else {
			return updateStat(oldStats.get(0), isValid);
		}
		
	}

	private Stat updateStat(Stat stat, boolean isValid) {
		if (isValid) { 
			stat.setRequestCount(stat.getRequestCount() + 1L);
		}
		else {
			stat.setInvalidCount(stat.getInvalidCount() + 1L);
		}
		log.log(Level.INFO, "Updating an existing stat");
		statRepository.save(stat);
		return stat;
	}

	private Stat insertStat(Integer customerId, Timestamp timeStamp, boolean isValid) {
		Stat newStat = new Stat();
		newStat.setCustomer(customerService.findCustomerByID(customerId));
		newStat.setTime(timeStamp);
		if (isValid) {
			newStat.setRequestCount(1L);
			newStat.setInvalidCount(0L);
		}
		else {
			newStat.setRequestCount(0L);
			newStat.setInvalidCount(1L);
		}
		log.log(Level.INFO, "Inserting a new stat");
		statRepository.save(newStat);
		return newStat;
	}


}
