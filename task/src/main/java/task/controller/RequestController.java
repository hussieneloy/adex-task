package task.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import task.dto.StatResponse;
import task.model.Customer;
import task.model.IPBlacklist;
import task.model.UABlacklist;
import task.service.CustomerService;
import task.service.IPBlacklistService;
import task.service.RequestService;
import task.service.StatService;
import task.service.UABlacklistService;

@RestController
@RequestMapping(value = "/api")
public class RequestController {
	@Autowired
	CustomerService customerService;
	@Autowired
	IPBlacklistService ipBlacklistService;
	@Autowired
	UABlacklistService uaBlacklistService;
	@Autowired
	RequestService requestService;
	@Autowired
	StatService statService;
	
	@GetMapping(value = "/customer/all")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Customer>> getAllCustomers() {
		return new ResponseEntity<List<Customer>>(customerService.getAllCustomers(), HttpStatus.OK);
	}
	
	@PostMapping(value = "/customer/create")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
		return new ResponseEntity<Customer>(customerService.createCustomer(customer), HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/ip/all")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<IPBlacklist>> getBlacklistIPs() {
		return new ResponseEntity<List<IPBlacklist>>(ipBlacklistService.getAllIps(), HttpStatus.OK);
	}
	
	@PostMapping(value = "/ip/create")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<IPBlacklist> createIP(@RequestBody IPBlacklist ipBlacklist) {
		return new ResponseEntity<IPBlacklist>(ipBlacklistService.createIP(ipBlacklist), HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/ua/all")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<UABlacklist>> getBlacklistUAs() {
		return new ResponseEntity<List<UABlacklist>>(uaBlacklistService.getAllUAs(), HttpStatus.OK);
	}
	
	@PostMapping(value = "/ua/create")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<UABlacklist> createUA(@RequestBody UABlacklist uaBlacklist) {
		return new ResponseEntity<UABlacklist>(uaBlacklistService.createUA(uaBlacklist), HttpStatus.CREATED);
	}
	
	@PostMapping(value = "/request/create")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<String> createRequest(@RequestBody String rawJson) {
		return new ResponseEntity<String>(requestService.handleRequest(rawJson), HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/stat")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<StatResponse> getStat(@RequestParam Integer customerId, @RequestParam String day) {
		return new ResponseEntity<StatResponse>(statService.getStat(customerId, day), HttpStatus.OK);
		
	}
	
}
