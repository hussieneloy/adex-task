package task.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

@Entity(name = "UABlacklist")
@Table(name = "ua_blacklist")
public class UABlacklist {
	
	@Id
	@NonNull
	private String ua;

	public String getUa() {
		return ua;
	}

	public void setUa(String ua) {
		this.ua = ua;
	}
	

}
