package task.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.lang.NonNull;

@Entity(name = "Stat")
@Table(name = "hourly_stats", indexes = 
{@Index(name = "customer_idx", columnList = "customer_id")}, 
uniqueConstraints = {@UniqueConstraint (name = "unique_customer_time", columnNames = { "customer_id", "time" })})
public class Stat {
	@Id
	@GeneratedValue
	@NonNull
	private Integer id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "customer_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Customer customer;
	
	@NonNull
	@Column(name = "time")
	private Timestamp time;
	
	@NonNull
	@Column(name = "request_count")
	private Long requestCount;
	
	@NonNull
	@Column(name = "invalid_count")
	private Long invalidCount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public Long getRequestCount() {
		return requestCount;
	}

	public void setRequestCount(Long requestCount) {
		this.requestCount = requestCount;
	}

	public Long getInvalidCount() {
		return invalidCount;
	}

	public void setInvalidCount(Long invalidCount) {
		this.invalidCount = invalidCount;
	}
	
	
	
	
	
	

}
