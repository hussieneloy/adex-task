package task.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

@Entity(name = "IPBlacklist")
@Table(name = "ip_blacklist")
public class IPBlacklist {
	
	@Id
	@NonNull
	private Long ip;

	public Long getIp() {
		return ip;
	}

	public void setIp(Long ip) {
		this.ip = ip;
	}
	
}
