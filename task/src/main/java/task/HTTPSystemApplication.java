package task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"task.repository", "task.model", "task", "task.model"})
public class HTTPSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(HTTPSystemApplication.class, args);
	}
}
