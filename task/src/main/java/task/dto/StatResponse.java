package task.dto;

import java.sql.Date;

public class StatResponse {
	private Integer customerId;
	private Date day;
	private Long customerValidRequests;
	private Long customerInvalidRequests;
	private Long totalInvalidRequests;
	private Long totalValidRequests;
	
	public StatResponse(Integer customerId, Date day, Long customerValidRequests, Long customerInvalidRequests,
			Long totalInvalidRequests, Long totalValidRequests) {
		super();
		this.customerId = customerId;
		this.day = day;
		this.customerValidRequests = customerValidRequests;
		this.customerInvalidRequests = customerInvalidRequests;
		this.totalInvalidRequests = totalInvalidRequests;
		this.totalValidRequests = totalValidRequests;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Date getDay() {
		return day;
	}
	public void setDay(Date day) {
		this.day = day;
	}
	public Long getCustomerValidRequests() {
		return customerValidRequests;
	}
	public void setCustomerValidRequests(Long customerValidRequests) {
		this.customerValidRequests = customerValidRequests;
	}
	public Long getCustomerInvalidRequests() {
		return customerInvalidRequests;
	}
	public void setCustomerInvalidRequests(Long customerInvalidRequests) {
		this.customerInvalidRequests = customerInvalidRequests;
	}
	public Long getTotalInvalidRequests() {
		return totalInvalidRequests;
	}
	public void setTotalInvalidRequests(Long totalInvalidRequests) {
		this.totalInvalidRequests = totalInvalidRequests;
	}
	public Long getTotalValidRequests() {
		return totalValidRequests;
	}
	public void setTotalValidRequests(Long totalValidRequests) {
		this.totalValidRequests = totalValidRequests;
	}
	
	

}
