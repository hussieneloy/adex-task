package task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import task.model.IPBlacklist;

@Repository
public interface IPBlacklistRepository extends JpaRepository<IPBlacklist, Long> {

}
