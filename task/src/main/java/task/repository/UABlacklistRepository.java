package task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import task.model.UABlacklist;

@Repository
public interface UABlacklistRepository extends JpaRepository<UABlacklist, String> {

}
