package task.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import task.model.Stat;

@Repository
public interface StatRepository extends JpaRepository<Stat, Integer> {
	
	static final String SINGLE_STAT_QUERY = "SELECT s FROM Stat s WHERE s.customer.id = ?1 AND s.time = ?2";
	static final String TOTAL_VALID = "SELECT Sum(s.requestCount) FROM Stat s WHERE s.time >= ?1 AND s.time <= ?2";
	static final String TOTAL_INVALID = "SELECT Sum(s.invalidCount) FROM Stat s WHERE s.time >= ?1 AND s.time <= ?2";
	static final String CUSTOMER_VALID = "SELECT Sum(s.requestCount) FROM Stat s WHERE s.time >= ?1 AND s.time <= ?2 AND "
			+ "s.customer.id = ?3";
	static final String CUSTOMER_INVALID = "SELECT Sum(s.invalidCount) FROM Stat s WHERE s.time >= ?1 AND s.time <= ?2 AND "
			+ "s.customer.id = ?3";
	
	@Query(SINGLE_STAT_QUERY)
	List<Stat> getByCustomerAndTimestamp(Integer customerId, Timestamp timeStamp);
	
	@Query(TOTAL_VALID)
	Long getTotalValidRequests(Timestamp start, Timestamp end);
	
	@Query(TOTAL_INVALID)
	Long getTotalInvalidRequests(Timestamp start, Timestamp end);

	@Query(CUSTOMER_VALID)
	Long getCustomerValidRequests(Timestamp start, Timestamp end, Integer customerId);

	@Query(CUSTOMER_INVALID)
	Long getCustomerInvalidRequests(Timestamp start, Timestamp end, Integer customerId);

}
