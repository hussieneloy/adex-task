- The task was implement using Java + Spring boot and using Postgres Sql as the database.
- The project can be run only using Maven and Docker. No further installations are needed.
- To run the project. Go into the main directory (where pom.xml is located) and execute $ mvn clean install
- After a successful build, execute $ docker-compose build && docker-compose up. 
- Wait for a while until all the required images are installed and the containers are created.
- The pages should be available at port 8080.
- For customers, ua_blacklist and ip_blacklist, there exist 2 pages for each entity 
where the user could add new ones using "entity/create" and view all as "entity/all"
- I did the creation using JSON post requests.
- To add a Customer as in example, visit "http://localhost:8080/api/customer/create" and send POST request:
''' {"name": "Big News Media Corp", "active":true} '''
- To add an IP Blacklist as in example, visit "http://localhost:8080/api/ip/create" and send POST request:
''' {"ip": 0} '''
- To add a UA Blacklist as in example, visit "http://localhost:8080/api/ua/create" and send POST request:
''' {"ua": "Googlebot"} '''
- To view the current data situation replace the word "create" by "all".
- In order to send a request, visit "http://localhost:8080/api/request/create" and send POST request like the one included in the task description.
- In case of invalid requests, you will be informed with the reason of the invalidity. Otherwise, you will be informed with a successful request.
- I assumed that IPs would be in IPv6 format as strings in the requests but stored as longs in the db. 
- I assumed that the UAs are corresponding to the customer names. 
- I assumed that the timestamp would be added to the request in the form of unix timestamp and not at the actual time of the request.
- I assumed that hour means a specific timestamp and not an exact hour with no minutes and seconds because that would mean plenty of rounding 
which was not included in the description. 
- I could not find a use for 'userID' and 'tagID' fields but they have to be included nonetheless for the request to be valid.
- In order to to view the stats about a specific customer and date, visit "http://localhost:8080/api/stat?customerId=?&day=????-??-??"
- The date must be in "yyyy-MM-dd" format.
- The response of the the stats would be be in form of a json object as no mention of a UI was included in the description.
 - In order to see the information resulted from the request in the description visit: "http://localhost:8080/api/stat?customerId=1&day=2017-7-14".
 
- The initial part of containerizing Spring and Postgres using Docker was heavily influenced by the example present here: https://github.com/shameed1910/spring-postgres-docker

